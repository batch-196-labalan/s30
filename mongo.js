// 
// db.fruits.insertMany(
//  [
//     {
//      "name": "Apple",
//      "supplier": "Red Farms Inc.",
//      "stocks": 20,
//      "price": 40,
//      "onSale": true
//     },
//     {
//      "name": "Banana",
//      "supplier": "Yellow Farms",
//      "stocks": 15,
//      "price": 20,
//      "onSale": true
//     },
//     {
//      "name": "Kiwi",
//      "supplier": "Green Farming and Canning",
//      "stocks": 25,
//      "price": 50,
//      "onSale": true
//     },
//     {
//      "name": "Mango",
//      "supplier": "Yellow Farms",
//      "stocks": 10,
//      "price": 60,
//      "onSale": true
//     },
//      {
//      "name": "Dragon Fruit",
//      "supplier": "Red Farms Inc.",
//      "stocks": 10,
//      "price": 60,
//      "onSale": true
//     }
//   ]
// )
// 
// 

//agregate - needed when application need a form of information that is not visibly available

//Aggregation Pipeline Stages (2 - 3 steps)



db.fruits.aggregate([

    {$match: {onSale:true}},
    {$group: {_id: "$supplier", totalStocks: {$sum: "$stocks"}}}

])



//total but not group
db.fruits.aggregate([

    {$match: {onSale:true}},
    {$group: {_id: "AllFruits", totalStocks: {$sum: "$stocks"}}}

])


db.fruits.aggregate([

    {$match: {supplier: "Red Farms Inc."}},
    {$group: {_id: "RedFarmsInc", totalStocks: {$sum: "$stocks"}}}

])


//MINI ACATIVITY

// db.fruits.aggregate([
// 
//     {$match: {onSale:true, supplier: "Yellow Farms"}},
//     {$group: {_id: "Yellow Farms", totalStocks: {$sum: "$stocks"}}}
// 
// ])
 
    //-------O-----R------

db.fruits.aggregate([
 {$match: {$and:[{supplier: "Yellow Farms"},{onSale:true}]}},
 {$group: {_id: "$supplier", totalStocks: {$sum: "$stocks"}}}
])
 
 
 
 
//$avg - is an operator used in $group stage. 
 
 db.fruits.aggregate([
 
    {$match: {onSale:true}},
    {$group:{_id: "$supplier", avgStock: {$avg:"$stocks"}}}
 
 ])
    
 
 db.fruits.aggregate([
 
    {$match: {onSale:true}},
    {$group:{_id: null, avgPrice: {$avg:"$price"}}}
 
 ])
    
    
    
    
//$max - will allow us to get the highest value out of all the values in a given field per group.
    
  db.fruits.aggregate([
 
    {$match: {onSale:true}},
    {$group:{_id: "highestyStockOnSale", maxStock: {$max:"$stocks"}}}
 
 ])
    
    
db.fruits.aggregate([
 
    {$match: {onSale:true}},
    {$group:{_id: null, maxPrice: {$max:"$price"}}}
 
 ])
    
 
    
      
//$min - gets the lowest value
    
 db.fruits.aggregate([
 
    {$match: {onSale:true}},
    {$group:{_id: "lowestStockOnSale", minStock: {$min:"$stocks"}}}
 
 ])
        
 
 db.fruits.aggregate([
 
    {$match: {onSale:true}},
    {$group:{_id: "lowestPriceOnSale", minPrice: {$min:"$price"}}}
 
 ])  
  
  
  
  
//MINI ACTIVITY
  
   db.fruits.aggregate([
   {$match: {price: {$lt:50}}},
   {$group:{_id: "lowestStock", minStock: {$min:"$stocks"}}}
 
 ])  
   
   
   
   
//$count - added after $match stage to count all items that matches criteria
  
db.fruits.aggregate([
   {$match: {onSale:true}},
   {$count: "itemsOnSale"}
 
 ])  
 


db.fruits.aggregate([
   
   {$match: {price:{$lt:50}}},
   {$count: "itemsPriceLessThan50"}
 
 ])     


db.fruits.aggregate([
   
   {$match: {onSale:true}},
   {$count: "forRestock"}
 
 ])       




//$out - save the result in a new collection
//note: this will override the collection if it already exists
   
db.fruits.aggregate([
   
   {$match: {onSale:true}},
   {$group:{_id: "$supplier", totalStocks: {$sum:"$stocks"}}},
   {$out: "stocksPerSupplier"}
 
 ])    


//ACTIVITY
 
//1.
db.fruits.aggregate([
 
  {$match: {$and:[{supplier: "Yellow Farms"},{onSale:true}, {price:{$lt:50}}]}},
   {$count: "totalNunmberOfItemsByYellowFarm"}
 
 ])

   
   
//2.   
db.fruits.aggregate([
   
   {$match: {price:{$lt:30}}},
   {$count: "itemsPriceLessThan30"}
 
 ])
   
//3.
db.fruits.aggregate([
 
    {$match: {onSale:true, supplier: "Yellow Farms"}},
    {$group:{_id: "$supplier", avgPrice: {$avg:"$price"}}}
 
 ])
    
//4.
db.fruits.aggregate([
 
    {$match: {onSale:true, supplier: "Red Farms Inc."}},
    {$group:{_id: "$supplier", maxPrice: {$max:"$price"}}}
 
 ])  
  
  
  
//5.  
db.fruits.aggregate([
 
    {$match: {onSale:true, supplier: "Red Farms Inc."}},
    {$group:{_id: "$supplier", minPrice: {$min:"$price"}}}
 
 ])  
      

   

   
    
    