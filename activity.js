//ACTIVITY
 
//1.
db.fruits.aggregate([
 
  {$match: {$and:[{supplier: "Yellow Farms"},{onSale:true}, {price:{$lt:50}}]}},
   {$count: "totalNunmberOfItemsByYellowFarm"}
 
 ])

   
   
//2.   
db.fruits.aggregate([
   
   {$match: {price:{$lt:30}}},
   {$count: "itemsPriceLessThan30"}
 
 ])
   
//3.
db.fruits.aggregate([
 
    {$match: {onSale:true, supplier: "Yellow Farms"}},
    {$group:{_id: "$supplier", avgPrice: {$avg:"$price"}}}
 
 ])
    
//4.
db.fruits.aggregate([
 
    {$match: {onSale:true, supplier: "Red Farms Inc."}},
    {$group:{_id: "$supplier", maxPrice: {$max:"$price"}}}
 
 ])  
  
  
  
//5.  
db.fruits.aggregate([
 
    {$match: {onSale:true, supplier: "Red Farms Inc."}},
    {$group:{_id: "$supplier", minPrice: {$min:"$price"}}}
 
 ])  
      
